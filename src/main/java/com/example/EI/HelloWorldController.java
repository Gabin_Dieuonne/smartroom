package com.example.EI;

import org.json.JSONObject;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

@RestController
public class HelloWorldController {
    @GetMapping("/")
    public String hello() {

        String res;
        JdbcMysqlJ8IntegrationTests j = new JdbcMysqlJ8IntegrationTests();
        try {
            res = j.pooledConnectionTest();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return res;
    }
    @GetMapping("/{nomCapteur}/{tsDebut}/{tsFin}")
    public Vector<Map<String, Object>> getData(@PathVariable String nomCapteur, @PathVariable long tsDebut, @PathVariable long tsFin){
        Vector<Map<String, Object>> donnees = new Vector<Map<String, Object>>();
        JdbcMysqlJ8IntegrationTests j = new JdbcMysqlJ8IntegrationTests();
        try{
            j.connection();
            Connection con = j.connectionPool.getConnection();
            ResultSet rs = j.getResults(nomCapteur, tsDebut, tsFin, con);
            while(rs.next()){
                Map<String, Object> m = new HashMap<String, Object>();
                String idCapteur = rs.getString("IdCapteur");
                m.put("IdCapteur", idCapteur);
                long ts = rs.getLong("Instant");
                m.put("Timestamp", ts);
                Float valeur = rs.getFloat("Valeur");
                m.put("Valeur", valeur);
                donnees.add(m);
            }

            con.close();
        }
        catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return donnees;
    }
    @GetMapping("/drop")
    public Boolean drop(){
        try {
            JdbcMysqlJ8IntegrationTests j = new JdbcMysqlJ8IntegrationTests();
            j.connection();
            j.dropTableIfPresent();
            j.setUpPool();
            return true;
        }
        catch(Exception e){
            System.out.println(e);
            return false;
        }
    }
}

