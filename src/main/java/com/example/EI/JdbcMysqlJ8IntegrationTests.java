package com.example.EI;

import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth.assertWithMessage;

import com.google.common.collect.ImmutableList;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.json.JSONObject;

import java.sql.*;
import java.util.*;
import java.util.concurrent.TimeUnit;


public class JdbcMysqlJ8IntegrationTests {
    private static final String CONNECTION_NAME = "smartroomzigbee:europe-west1:bdd-ei-smartroom"; // nom complet de l'instance de BDD chez Google
    private static final String DB_NAME = "bdd-ei"; // Nom de la BDD
    private static final String DB_USER = "gabin"; // utilisateur
    private static final String DB_PASSWORD = "mdp"; // password

    private String tableName;
    public HikariDataSource connectionPool;
    public void connection() throws SQLException{
        // Set up URL parameters
        String jdbcURL = String.format("jdbc:mysql:///%s", DB_NAME);
        Properties connProps = new Properties();
        connProps.setProperty("user", DB_USER);
        connProps.setProperty("password", DB_PASSWORD);
        connProps.setProperty("socketFactory", "com.google.cloud.sql.mysql.SocketFactory");
        connProps.setProperty("cloudSqlInstance", CONNECTION_NAME);

        // Initialize connection pool
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(jdbcURL);
        config.setDataSourceProperties(connProps);
        config.setConnectionTimeout(2000); // 10s

        this.connectionPool = new HikariDataSource(config);
        this.tableName = "Capteurs";
    }

    public void setUpPool() throws SQLException {

        // Create table
        try (Connection conn = connectionPool.getConnection()) {
            String stmt = String.format("CREATE TABLE %s (", this.tableName)
                    + "  IdCapteur VARCHAR(50) NOT NULL,"
                    + "  Instant BIGINT,"
                    + "  Valeur FLOAT"
                    + ");";
            try (PreparedStatement createTableStatement = conn.prepareStatement(stmt)) {
                createTableStatement.execute();
            }
        }
    }

    public void dropTableIfPresent() throws SQLException {
        try (Connection conn = connectionPool.getConnection()) {
            String stmt = String.format("DROP TABLE %s;", this.tableName);
            try (PreparedStatement dropTableStatement = conn.prepareStatement(stmt)) {
                dropTableStatement.execute();
            }
        }
    }


    public String pooledConnectionTest() throws SQLException {
        connection();
        List<String> bookList = new ArrayList<>();
        try (Connection conn = connectionPool.getConnection()) {
            String stmt = String.format("SELECT * FROM %s", this.tableName);
            try (PreparedStatement selectStmt = conn.prepareStatement(stmt)) {
                selectStmt.setQueryTimeout(10); // 10s
                ResultSet rs = selectStmt.executeQuery();
                while (rs.next()) {
                    bookList.add(String.valueOf(rs.getString(1)));
                    bookList.add(String.valueOf(rs.getLong(2)));
                    bookList.add(String.valueOf(rs.getFloat(3)));
                }
            }
        }
        return bookList.toString();
    }
    public ResultSet getResults(String idCapteur, long tsDebut, long tsFin, Connection con) throws SQLException{
        try{
            PreparedStatement stmt = con.prepareStatement(String.format("SELECT * FROM %s WHERE IdCapteur = ? AND Instant > ? AND Instant < ?", this.tableName));
            stmt.setString(1, idCapteur);
            stmt.setLong(2, tsDebut);
            stmt.setLong(3, tsFin);
            ResultSet rs = stmt.executeQuery();
            return rs;
        }
        catch(Exception e){
            System.out.println(e);
            return null;
        }
    }
    public void insertData(Vector<Object> param1) throws SQLException {
        connection();
        try(Connection con = connectionPool.getConnection(); PreparedStatement stmt = con.prepareStatement(String.format("INSERT INTO %s VALUES(?,?,?)", this.tableName));){
            for (Object obj : param1) {
                HashMap<String, Object> jsonObject = (HashMap<String, Object>) obj;
                String idCapteur = (String) jsonObject.get("sensorName");
                long ts = (long) jsonObject.get("timestamp");
                double value = (double) jsonObject.get("value");
                stmt.setString(1, idCapteur);
                stmt.setLong(2, ts);
                stmt.setDouble(3, value);
                stmt.addBatch();
                stmt.executeBatch();
            }
        }
        catch(Exception e){
            System.out.println(e);
        }

    }
}
