package com.example.EI;

import org.json.JSONArray;
import org.json.JSONObject;
import org.apache.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;
import java.util.Vector;

@RestController
public class PostInt {
    @RequestMapping(value = "/capteur", method = RequestMethod.POST)
    @ResponseBody
    public void testInt(@RequestBody Vector<Object> param1) {
    try{
        JdbcMysqlJ8IntegrationTests j = new JdbcMysqlJ8IntegrationTests();
        j.insertData(param1);
        System.out.println(param1);
    }
    catch(Exception e){
        System.out.println(e);
    }
    }
}
